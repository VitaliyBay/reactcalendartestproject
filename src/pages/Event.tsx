import React, { FC } from 'react';
import { Layout, Row, Button, Modal } from 'antd';
import EventCalendar from './../components/EventCalaendar';
import EventForm from './../components/EventForm';
import { useState } from 'react';
import { useAction } from '../hooks/useAction';
import { useEffect } from 'react';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { IEvent } from '../models/IEvent';

const Event: FC = () => {

    const [modalVisisble, setModalVisible] = useState(false);
    const {fetchGuests, createEvent, fetchEvents} = useAction();
    const {guests, events} = useTypedSelector(state => state.event)
    const {user} = useTypedSelector(state => state.auth)

    useEffect(() => {
        fetchGuests();
        fetchEvents(user.username);
    }, [])

    const addNewEvent = (event: IEvent) => {
        setModalVisible(false);
        createEvent(event, user.username);
    }

    return (
        <Layout>
            <EventCalendar events={events}/>
            <Row justify='center'>
                <Button onClick={() => setModalVisible(true)}>Add Event</Button>
            </Row>
            <Modal title='Add Event' 
                   visible={modalVisisble} 
                   footer={null} onCancel={() => setModalVisible(false)}
            >
                <EventForm guests={guests} 
                           submit={addNewEvent}/>
            </Modal>
        </Layout>
    )
}

export default Event;