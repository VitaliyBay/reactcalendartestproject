import React, { FC, useState } from 'react';
import { Form, Input, Button } from 'antd';
import { rules } from '../utils/rules';
import { useDispatch } from 'react-redux';
import { AuthActionCreators } from '../store/reducers/auth/action-creaters';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAction } from '../hooks/useAction';

const LoginForm: FC = () => {
    
    const {login} = useAction();
    const {isLoading, error} = useTypedSelector(state => state.auth);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const submit = () => {
        login(username, password)
    };
    
    return (
        <Form
            onFinish={submit}
        > 
            {error && <div style={{color: 'red'}}>
                {error}
            </div>}
            <Form.Item label='User Name' name='username' rules={[rules.required("Please enter username")]}>
                <Input value={username} onChange={e => setUsername(e.target.value)} />
            </Form.Item>

            <Form.Item label='Password' name='password' rules={[rules.required("Please enter password")]}>
                <Input value={password} onChange={e => setPassword(e.target.value)} type='password' />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit" loading={isLoading}>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    )
}

export default LoginForm;