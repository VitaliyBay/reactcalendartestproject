import React from 'react';
import { FC } from 'react';
import { Calendar } from 'antd';
import { IEvent } from '../models/IEvent';
import { Moment } from 'moment';
import { formateDate } from '../utils/date';

interface EventCalendarProps {
    events: IEvent[]
}

const EventCalendar: FC<EventCalendarProps> = (props) => {
    function dateCellRender(value: Moment) {
        const formatedDate = formateDate(value.toDate());
        const currentDateEvents = props.events.filter(event => event.date === formatedDate);
        return (
            <div>
                {currentDateEvents.map((event, index) => <div key={index}>{event.description}</div>)}
            </div>
        );
      }

    return (
        <Calendar dateCellRender={dateCellRender}/>
    )
}

export default EventCalendar;