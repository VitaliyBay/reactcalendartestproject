import React, { FC } from 'react';
import { Layout, Row, Menu } from 'antd';
import { useHistory } from 'react-router-dom';
import { RouteName } from '../router';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAction } from '../hooks/useAction';

const Navbar: FC = () => {
    const router = useHistory()
    const {isAuth, user} = useTypedSelector(state => state.auth);
    const {logout} = useAction();

    return (
        <Layout.Header>
            <Row justify="end">
                {isAuth
                    ? 
                    <>
                        <div style={{color: 'white', margin: '0 10px 0 0'}}>{user.username}</div>
                        <Menu theme='dark' mode='horizontal' selectable={false}>
                            <Menu.Item onClick={() => logout()} key={1}>Logout</Menu.Item>
                        </Menu>
                    </>
                    :
                    <Menu theme='dark' mode='vertical' selectable={false}>
                        <Menu.Item onClick={() => router.push(RouteName.LOGIN)} key={1}>Login</Menu.Item>
                    </Menu>
                }
            </Row>
        </Layout.Header>
    )
}

export default Navbar;