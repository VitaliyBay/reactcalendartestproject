import { EventAction, EventActionName, EventState } from './types';

const initialState: EventState = {
    guests: [],
    events: []
}

export default function EventReducer(state = initialState, action: EventAction): EventState {
    switch(action.type) {
        case EventActionName.SET_GUEST:
            return {...state, guests: action.payload}
        case EventActionName.SET_EVENT:
            return {...state, events: action.payload}
        default: 
            return state; 
    }
}