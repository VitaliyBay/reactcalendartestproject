import { AppDispatch } from './../../index';
import { IEvent } from './../../../models/IEvent';
import { SetGuestsAction, EventActionName, SetEventsAction } from './types';
import { IUser } from './../../../models/IUser';
import UserService from '../../../api/UserService';

export const EventActionCreators = {
    setGuests: (users: IUser[]): SetGuestsAction => ({type: EventActionName.SET_GUEST, payload: users}),
    setEvents: (events: IEvent[]): SetEventsAction => ({type: EventActionName.SET_EVENT, payload: events}),
    fetchGuests: () => async (dispatch: AppDispatch) => {
        try {
            const response = await UserService.getUsers();
            dispatch(EventActionCreators.setGuests(response.data))
        } catch (error) {
            console.log(error);
        }
    },
    createEvent: (event: IEvent, username: string) => async (dispatch: AppDispatch) => {
        try {
            const events = localStorage.getItem('events') || '[]';
            const json = JSON.parse(events) as IEvent[];
            json.push(event);
            const currentUserEvents = json.filter(event => event.author === username || event.guest === username);
            dispatch(EventActionCreators.setEvents(currentUserEvents));
            localStorage.setItem('events', JSON.stringify(json));
        } catch (error) {
            console.log(error);
        }
    },
    fetchEvents: (username: string) => async (dispatch: AppDispatch) => {
        try {
            const events = localStorage.getItem('events') || '[]';
            const json = JSON.parse(events) as IEvent[];
            const currentUserEvents = json.filter(event => event.author === username || event.guest === username);
            dispatch(EventActionCreators.setEvents(currentUserEvents));
        } catch (error) {
            
        }
    }
}